package ru.mirea

import com.sun.jersey.client.impl.CopyOnWriteHashMap
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.LongWritable
import org.apache.hadoop.io.Text
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.Mapper
import org.apache.hadoop.mapreduce.Reducer
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat
import org.apache.hadoop.util.GenericOptionsParser
import org.apache.hadoop.util.Tool
import org.apache.hadoop.util.ToolRunner
import java.io.IOException
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicLong
import kotlin.system.exitProcess

class MyMapper : Mapper<Any, Text, LongWritable, Text>() {

    private lateinit var model: Model

    private val isHeader = AtomicBoolean(true)
    private val keyId = AtomicLong(0L)

    override fun setup(context: Context?) {
        super.setup(context)

        model = Model()
    }

    @Throws(IOException::class, InterruptedException::class)
    override fun map(key: Any, value: Text, context: Context) {
        if (isHeader.getAndSet(false)) return

        value.toString()
            .split(',')
            .let { row ->
                model.mapAndGetDoubleOrNull(row)
                    ?.let { targetValue ->
                        context.write(LongWritable(keyId.addAndGet(1L)), Text(targetValue.toString()))
                    }
            }
    }
}

class MyReducer : Reducer<LongWritable, Text, Text, Text>() {

    private val counts = CopyOnWriteHashMap<String, Int>()

    @Throws(IOException::class, InterruptedException::class)
    override fun reduce(
        key: LongWritable?,
        values: MutableIterable<Text>?,
        context: Context?
    ) {
        // Aggregate the counts
        values?.forEach { value ->
            value.toString().trim().let {
                counts[it] = counts.getOrDefault(it, 0) + 1
            }
        }
    }

    @Throws(IOException::class, InterruptedException::class)
    override fun cleanup(context: Context?) {
        context?.write(Text("Cleanup"), Text(counts.entries.size.toString()))

        super.cleanup(context)
    }

    override fun run(context: Context?) {
        super.run(context)

        // Write each key (target column value) and amount of it appearances (counts)
        counts.forEach { (key, count) ->
            context?.write(
                Text(key),
                Text(count.toString())
            )
        }

        ProcessBuilder("pwd")
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
    }
}

class DriverBigData : Configuration(), Tool {
    override fun setConf(p0: Configuration?) {}

    override fun getConf(): Configuration = this

    override fun run(args: Array<out String>?): Int {
        val remainingArgs = GenericOptionsParser(conf, args).remainingArgs
        if (remainingArgs.size != 2) {
            error("Expected <input_path> <output_path>, but received ${remainingArgs.size} elements.")
        }

        Job.getInstance(this.conf, "Custom Kotlin Job")
            .apply {
                // Specifies the class of the Driver for this job
                setJarByClass(DriverBigData::class.java)

                mapperClass = MyMapper::class.java
                reducerClass = MyReducer::class.java

                // Specifies the job`s format for input
                inputFormatClass = TextInputFormat::class.java

                // Specifies the job`s format for output
                outputKeyClass = LongWritable::class.java
                outputValueClass = Text::class.java
            }.let { job ->
                FileInputFormat.addInputPath(job, Path(remainingArgs[0]))
                FileOutputFormat.setOutputPath(job, Path(remainingArgs[1]))

                return if (job.waitForCompletion(true)) 0 else 1
            }
    }

}

fun main(args: Array<String>) {
    exitProcess(ToolRunner.run(Configuration(), DriverBigData(), args))
}
