package ru.mirea

import java.io.InputStreamReader
import java.io.BufferedReader
import java.io.IOException
import java.io.OutputStreamWriter

class AgeOutlineRemovalModel {
//    private val targetIndex = records[0].indexOf("72.0") // Humidity(%)

    private val targetIndex = testRecords[0].lastIndex // "Game Length"

    private val data = testRecords.map { it[targetIndex].toInt() }
    private val q1: Int
    private val q3: Int
    private val lowerBound: Double
    private val upperBound: Double

    init {
        val sortedData = data.sorted()
        val q1Index = (sortedData.size * 0.25).toInt()
        val q3Index = (sortedData.size * 0.75).toInt()

        q1 = sortedData[q1Index]
        q3 = sortedData[q3Index]
        val iqr = q3 - q1
        lowerBound = q1 - 1.5 * iqr
        upperBound = q3 + 1.5 * iqr
    }

    fun getValueWithRemovedOutlines(row: List<String>): String? =
        if (row[targetIndex].toDouble() in lowerBound..upperBound) {
            row[targetIndex]
        } else {
            null
        }

    fun getObjectWithRemovedOutlines(row: List<String>): List<String>? =
        if (row[targetIndex].toDouble() in lowerBound..upperBound) {
            row
        } else {
            null
        }
}

private val testRecords = listOf(
    listOf("1", "30"),
    listOf("2", "29"),
    listOf("3", "31"),
    listOf("4", "16"),
    listOf("5", "24"),
    listOf("6", "29"),
    listOf("7", "28"),
    listOf("8", "117"),
    listOf("9", "42"),
    listOf("10", "23")
)

fun main() =
    try {
        val model = AgeOutlineRemovalModel()

        BufferedReader(InputStreamReader(System.`in`)).use { bufferedReader ->
            OutputStreamWriter(System.out).use { writer ->
                // Reading headers from CSV file to Reducer
                bufferedReader.readLine()

                bufferedReader.lines().forEach { line ->
                    model.getObjectWithRemovedOutlines(
                        line.split(",")
                    )?.let { filteredLine ->
                        writer.write(filteredLine.toString() + "\n")
                        writer.flush()
                    }
                }
            }
        }
    } catch (ioe: IOException) {
        ioe.printStackTrace()
    }
