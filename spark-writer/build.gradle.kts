plugins {
    // Apply the org.jetbrains.kotlin.jvm Plugin to add support for Kotlin.
    alias(libs.plugins.jvm)
}

dependencies {
    // This dependency is used by the application.
    implementation(libs.guava)
    implementation(kotlin("stdlib"))

    // Apache Hadoop
    implementation(libs.hadoop.client)

    // Apache Spark
    implementation(libs.spark.core)
    implementation(libs.spark.sql)

    // Jackson
    implementation(libs.jackson)
}

tasks.jar {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
        attributes["Main-Class"] = "ru.mirea.SparkWriterKt"
    }
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    }) {
        exclude("META-INF/*.SF", "META-INF/*.DSA", "META-INF/*.RSA")
    }
    isZip64 = true
}