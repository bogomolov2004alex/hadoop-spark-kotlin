package ru.mirea

import org.apache.spark.sql.RowFactory
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.stream.Collectors

fun main() =
    try {
        // Define the schema for the data
        val schema = StructType(
            arrayOf(
                StructField("Temperature(F)", DataTypes.StringType, false, Metadata.empty()),
                StructField("Count", DataTypes.IntegerType, false, Metadata.empty())
            )
        )

        // Read the input data from stdin
        val inputData = BufferedReader(InputStreamReader(System.`in`)).use { bufferedReader ->           
            bufferedReader.lines()
                .map { it.split(" ") }
                .map { row ->
                    val targetColumnValue = row[0].trim()
                    val count = row[1].trim().toInt()
                    RowFactory.create(targetColumnValue, count)
                }
                .collect(Collectors.toList())
        }

        SparkSession.builder()
            .appName("ORC Writer")
            .master("local[*]")
            .getOrCreate()
            .use { sparkSession ->
                sparkSession
                    // Create a DataFrame from the input data
                    .createDataFrame(inputData, schema)
                    .apply {
                        // Show the DataFrame (for debugging purposes)
                        show()
                    }.run {
                        // Write the DataFrame to an ORC file
                        write()
                            .format("orc")
                            .mode("overwrite")
                            // Define the output path
                            .save("hdfs://localhost:9000/user/barsky/output/result.orc")
                    }
            }
    } catch (e: Exception) {
        e.printStackTrace()
    }
