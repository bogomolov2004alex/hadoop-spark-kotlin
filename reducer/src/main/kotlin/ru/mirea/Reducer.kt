package ru.mirea

import java.io.InputStreamReader
import java.io.BufferedReader
import java.io.OutputStreamWriter
import java.io.IOException
import java.io.InputStream
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.util.concurrent.TimeUnit

fun main() =
    try {
        val counts = mutableMapOf<Int, Int>()
        val objectMapper = jacksonObjectMapper()

        val targetColumn = "Dummy column"

        BufferedReader(InputStreamReader(System.`in`)).use { bufferedReader ->
            bufferedReader.lines().forEach { line ->
                val record: List<String> = objectMapper.readValue(line)

                record
                    .elementAt(record.lastIndex)
                    .toIntOrNull()
                    ?.let { columnValue ->
                        counts[columnValue] = counts.getOrDefault(columnValue, 0) + 1
                    }
            }
        }

        ProcessBuilder(
            "java", "-jar", "spark-writer-0.1.0-SNAPSHOT.jar"
        ).redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
            .apply {
                OutputStreamWriter(outputStream).use { writer ->
                    counts.forEach { (columnValue, count) ->
                        writer.write(
                            objectMapper.writeValueAsString(
                                mapOf(targetColumn to columnValue, "Count" to count)
                            ) + "\n"
                        )

                        println("$columnValue\t$count")
                    }
                    writer.flush()
                }
            }.run { println("Waited for Spark Writer: ${waitFor(60, TimeUnit.MINUTES)}") }
    } catch (ioe: IOException) {
        ioe.printStackTrace()
    } catch (ie: InterruptedException) {
        ie.printStackTrace()
    }
